#-*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager
from django.db import models
#from django.utils.translation import ugettext_lazy as _




TYPE_GENDER_CHOICES = (
    ('M', '남자'),
    ('F', '여자'),
)


import datetime

class ColonUserManager(BaseUserManager):
    def create_user(
        self,
        username,
        first_name,
        last_name,
        email,
        age,
        gender,
        birthday,

        phone_number,
        school,
        major,
        homepage,
        twitter,
        facebook,
        introduction,
        password=None
    ):
        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            age=age,
            gender=gender,
            birthday=birthday,

            phone_number=phone_number,
            school=school,
            major=major,
            homepage=homepage,
            twitter=twitter,
            facebook=facebook,
            introduction=introduction,
        )

        print 'aaa'
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(
        self,
        username,
        password,
        first_name='ColonH',
        last_name='Admin',
        email='Webmaster.ColonH@gmail.com',
        age=100,
        gender='M',
        birthday=datetime.datetime.now().strftime("%Y-%m-%d"),

        phone_number='010-111-1111',
        school='Hong-ik Univ.',
        major='',
        homepage='',
        twitter='',
        facebook='',
        introduction='',
    ):
        user = self.create_user(
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            email=email,
            age=age,
            gender=gender,
            birthday=birthday,

            phone_number=phone_number,
            school=school,
            major=major,
            homepage=homepage,
            twitter=twitter,
            facebook=facebook,
            introduction=introduction,
        )
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)
        return user


class ColonUser(AbstractUser):
    # 이미 있는 것
    # username, first_name, last_name, email,
    # password, groups, user_permissions, 
    # is_staff, is_active, is_superuser,
    # last_login, date_joined
    age = models.IntegerField("나이")
    gender = models.CharField("성별", max_length=1, choices=TYPE_GENDER_CHOICES)
    birthday = models.DateField("생일")

    # 선택사항
    phone_number = models.CharField("휴대전화", max_length=15, blank=True)
    school = models.CharField("학교", max_length=100, blank=True)
    major = models.CharField("전공", max_length=100, blank=True)
    homepage = models.CharField("홈페이지", max_length=100, blank=True)
    twitter = models.CharField("트위터 계정", max_length=100, blank=True)
    facebook = models.CharField("페이스북 계정", max_length=100, blank=True)
    introduction = models.TextField("자기소개", blank=True)


    # 권한 
    '''
    기본적으로 제공되는 권한
    is_active
        자동가입을 막기 위한 메일 인증상태(django-registration)

    is_staff
        스태프 권한. 사무국 직원분들의 권한

    is_superuser
        관리자 권한. 개발자들의 권한
    '''
    objects = ColonUserManager()

    USERNAME_FIELD = 'username'


