from Volume.models import *
from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

class NumberAdmin(admin.ModelAdmin):
    list_display = ['title', 'number']

class PartAdmin(admin.ModelAdmin):
    list_display = ['title', 'description']

class ItemImageInline(admin.TabularInline):
    model = ItemImage
    extra = 3

class ItemAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Item

class ItemAdmin(admin.ModelAdmin):
    form = ItemAdminForm
    list_display = ['title', 'priority', 'part', 'shortTitle', 'number', 'created']
    # inlines = [ ItemImageInline, ]
    list_filter = ['number', 'part']

class NewsAdmin(admin.ModelAdmin):
    list_display = ['title']    

admin.site.register(Number, NumberAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(Item, ItemAdmin)


'''
from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

from post.models import Post

class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Post

class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm

admin.site.register(Post, PostAdmin)
'''