from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

class Number(models.Model):
    number = models.IntegerField()
    title = models.CharField(max_length=60)
    sub_title = models.CharField(max_length=200)
    priority = models.IntegerField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateField(blank=True, null=True)
    description = models.TextField(blank=True)
    thumbnail = models.ImageField(upload_to="volume/number/thumbnail", blank=True, null=True)
    image = models.ImageField(upload_to="volume/number/", blank=True, null=True)

    month = models.CharField(max_length=100, blank=True)

    def save(self):
        self.month = self.date.strftime("%B").upper()
        super(Number,self).save()

    def __unicode__(self):
        return u"%s : %s" % (self.number, self.title)

class Part(models.Model):
    title = models.CharField(max_length=60)
    url_title = models.CharField(max_length=60)
    priority = models.IntegerField(blank=True)
    created = models.DateTimeField(max_length=60)
    description = models.TextField(blank=True)
    thumbnail = models.ImageField(upload_to="volume/part/thumbnail", blank=True, null=True)
    image = models.ImageField(upload_to="volume/part/", blank=True, null=True)

    black_text_image = models.ImageField(upload_to="volume/part/", blank=True, null=True)
    gray_text_image = models.ImageField(upload_to="volume/part/", blank=True, null=True)
    white_text_image = models.ImageField(upload_to="volume/part/", blank=True, null=True)

    def __unicode__(self):
        return self.title

class Item(models.Model):
    priority = models.IntegerField(blank=True)
    number = models.ForeignKey(Number)
    created = models.DateTimeField(blank=True, null=True)
    part = models.ForeignKey(Part)
    title = models.CharField(max_length=60)
    sub_title = models.CharField(max_length=200)
    shortTitle = models.CharField(max_length=30, null=True)
    content = RichTextField()
    thumbnail = models.ImageField(upload_to="volume/item/thumbnail", blank=True, null=True)

    def __unicode__(self):
        return self.title


class ItemImage(models.Model):
    item = models.ForeignKey(Item, related_name="images")
    image = models.ImageField(upload_to="volume/item/")
    
    def encode_img(request, obj):
        if isinstance(obj, ImageFieldFile):
            try:
                return obj.path
            except ValueError, e:
                return ''
        raise TypeError(repr(obj) + " is not JSON serializable ItemImage")