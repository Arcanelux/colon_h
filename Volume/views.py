#-*- coding: utf-8 -*-
# Standard library - Related third-party -> Local application or library psecific
# Stdlib imports

# Core Django imports
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.core.paginator import InvalidPage, EmptyPage
from django.http import HttpResponse

# Third-party app imports
from flynsarmy_paginator.paginator import FlynsarmyPaginator as Paginator

# Imports from local apps
from Information.models import *
from Volume.models import *
from colonh.settings import MEDIA_URL



def get_menu_info():
    numbers = Number.objects.all().order_by('priority')
    items = Item.objects.all()
    parts = Part.objects.all().exclude(title=u'편집부')

    menu_info = {'numbers': numbers, 'items': items, 'parts': parts, 'media_url': MEDIA_URL}
    return menu_info

def issues_monthly(request):
    d = {}
    numbers = Number.objects.all().order_by('priority')

    d['media_url'] = MEDIA_URL
    d['numbers'] = numbers
    d['show_menu'] = False
    d['user'] = request.user

    return render_to_response("v2/issues_monthly.html", d)

def issues_parts(request, part_url_title = ''):
    d = {}
    cur_items = Item.objects.all()
    for part in Part.objects.all().exclude(title=u'편집부'):
        if part_url_title == part.url_title:
            cur_items = Item.objects.filter(part__url_title=part_url_title)
        # print part_url_title

    d['cur_items'] = cur_items
    d['parts'] = Part.objects.all().exclude(title=u'편집부').order_by('priority')
    d['media_url'] = MEDIA_URL
    d['cur_part_url_title'] = part_url_title
    d['show_menu'] = False
    d['user'] = request.user

    return render_to_response("v2/issues_parts.html", d)

def issue(request, num = -1):
    d = get_menu_info()
    if num == -1:
        cur_number = Number.objects.latest('number')
        num = cur_number.number
    else:
        cur_number = Number.objects.get(number=num)
    cur_items = Item.objects.filter(number__number=num).exclude(part__title=u'편집부').order_by("-priority").reverse()
    print num
    print cur_items
    d['cur_number'] = cur_number
    d['cur_items'] = cur_items
    d['show_menu'] = True
    d['user'] = request.user

    return render_to_response("v2/issue.html", d)

def issue_item(request, number, shortTitle):
    d = get_menu_info()

    item = Item.objects.get(number__number=number, shortTitle=shortTitle)
    images = item.images.all()

    d['item'] = item
    d['images'] = images
    d['media_url'] = MEDIA_URL
    d['user'] = request.user
    d['number'] = number
    d['short_title'] = shortTitle

    return render_to_response("v2/item.html", d)

