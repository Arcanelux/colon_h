from django.shortcuts import render_to_response
from django.core.paginator import InvalidPage, EmptyPage
from flynsarmy_paginator.paginator import FlynsarmyPaginator as Paginator

from django.core.urlresolvers import reverse

from Information.models import *
from Volume.models import *
from colonh.settings import MEDIA_URL

def main(request):
	numbers = Number.objects.all().order_by("-created")
	itemlist = []
	for num in numbers:
		items = Item.objects.filter(number=num)
		itemlist.append(items)
		
	number = numbers[0]
	items = Item.objects.filter(number=number.number)
	
	return render_to_response("v2/base.html", dict(user=request.user, curNumber=number, curItems=items, numbers=numbers, itemlist=itemlist, media_url=MEDIA_URL))
	
def about(request):
	### Menu Start ###
	numbers = Number.objects.all().order_by("-created")
	itemlist = []
	for num in numbers:
		items = Item.objects.filter(number=num)
		itemlist.append(items)
	d = dict(user=request.user, numbers=numbers, itemlist=itemlist)
	### Menu End ###

	return render_to_response("no_page.html", d)
	
def editors(request):
	### Menu Start ###
	numbers = Number.objects.all().order_by("-created")
	itemlist = []
	for num in numbers:
		items = Item.objects.filter(number=num)
		itemlist.append(items)
	d = dict(user=request.user, numbers=numbers, itemlist=itemlist)
	### Menu End ###

	return render_to_response("no_page.html", d)
	
def recruit(request):
	### Menu Start ###
	numbers = Number.objects.all().order_by("-created")
	itemlist = []
	for num in numbers:
		items = Item.objects.filter(number=num)
		itemlist.append(items)
	d = dict(user=request.user, numbers=numbers, itemlist=itemlist)
	### Menu End ###

	return render_to_response("no_page.html", d)
	
def contact(request):
	### Menu Start ###
	numbers = Number.objects.all().order_by("-created")
	itemlist = []
	for num in numbers:
		items = Item.objects.filter(number=num)
		itemlist.append(items)
	d = dict(user=request.user, numbers=numbers, itemlist=itemlist)
	### Menu End ###

	return render_to_response("no_page.html", d)