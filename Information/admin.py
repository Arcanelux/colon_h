from Information.models import *
from django.contrib import admin

class AboutAdmin(admin.ModelAdmin):
	list_display = ["title", "user", "created"]
    
	def save_model(self, request, obj, form, change):
		obj.author = request.user
		obj.save()

admin.site.register(About, AboutAdmin)
