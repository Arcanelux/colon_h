from django.db import models
from django.contrib.auth.models import User

try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

class About(models.Model):
	title = models.CharField(max_length=60)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, blank=True, null=True)
	image = models.ImageField(upload_to="about/", blank=True)
	content = models.TextField(blank=True)
	description = models.TextField(blank=True)

	def __unicode__(self):
		return self.title
		

class Editor(models.Model):
	name = models.CharField(max_length=60)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, blank=True, null=True)
	photo = models.ImageField(upload_to="editor/", blank=True)
	description = models.TextField(blank=True)
	
	def __unicode__(self):
		return self.name
		
class Recruiting(models.Model):
	title = models.CharField(max_length=60)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, blank=True, null=True)
	image = models.ImageField(upload_to="about/", blank=True)
	content = models.TextField(blank=True)
	description = models.TextField(blank=True)
	
	def __unicode__(self):
		return self.title
		
class Contact(models.Model):
	title = models.CharField(max_length=60)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, blank=True, null=True)
	image = models.ImageField(upload_to="about/", blank=True)
	content = models.TextField(blank=True)
	description = models.TextField(blank=True)

	def __unicode__(self):
		return self.title