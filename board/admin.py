from django.contrib import admin
from board.models import *

class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']

admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Category)