from django.db import models
from django.contrib.auth.models import User
from Volume.models import Part
from ckeditor.fields import RichTextField

# Custom user model
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

# Category
# Title
# Content
# Part
# Created
class Category(models.Model):
    title = models.CharField(max_length=100)
    description = RichTextField()

    def __unicode__(self):
        return self.title

class Post(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, blank=True, null=True)
    content = RichTextField()
    # part = models.ForeignKey(Part)
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return u"%s, %s" % (self.title, self.created)

class Comment(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    post = models.ForeignKey(Post, related_name='comments')
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.content