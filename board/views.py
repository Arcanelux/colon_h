#-*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from board.models import *
from board.forms import PostForm, CommentForm
from colonh.settings import MEDIA_URL

def board_qa(request):
    d = {}

    posts = Post.objects.all().order_by('-id')
    d['posts'] = posts
    d['user'] = request.user

    return render_to_response('v2/board_qa.html', d)

def board_qa_add(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            user = request.user
            title = form.cleaned_data['title']
            content = form.cleaned_data['content']
            category = form.cleaned_data['category']

            post = Post(user=user, title=title, content=content, category=category)
            post.save()

            return redirect('board_qa')
        else:
            return HttpResponse("Error")

    else:
        d = {}
        form = PostForm()

        d['form'] = form
        d['user'] = request.user
        return render_to_response('v2/board_qa_add.html', d, RequestContext(request))


def board_qa_item(request, post_id):
    if request.method == 'POST':
        query_dict = request.POST

        user = request.user
        post = Post.objects.get(id=post_id)
        content = query_dict['content']

        if content != '':
            new_comment = Comment(user=user, post=post, content=content)
            new_comment.save()

        d = {}

        return redirect('board_qa_item', post_id)
    else:
        d = {}

        post = Post.objects.get(id=post_id)
        comment_form = CommentForm()
        try:
            comments = Comment.objects.filter(post__id=post_id)
        except:
            comments = []

        d['post'] = post
        d['comment_form'] = comment_form
        d['comments'] = comments
        d['user'] = request.user
        print comments

        return render_to_response('v2/board_qa_item.html', d, RequestContext(request))

