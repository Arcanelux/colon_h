from django import forms
from django.forms import ModelForm
from board.models import Post, Comment

from ckeditor.widgets import CKEditorWidget

class PostForm(ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Post
        exclude = ['user', 'created']

class CommentForm(ModelForm):
    content = forms.CharField(widget=forms.Textarea(attrs={'style': 'width:100%; height:150px;'}))
    # content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Comment
        exclude = ['user', 'post', 'created']