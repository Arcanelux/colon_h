#!/usr/bin/python
#-*- coding: utf-8 -*-
import sys, os
from PIL import Image
import Image


def thumbnail(file):
    size = 300, 300
    outfile = file+"_thumbnail"
    try:
        im = Image.open(file)
        im.thumbnail(size)
        im.save(outfile, "JPEG")
    except IOError:
        print "cannot create thumbanil for ", file

def split(file):
    filename, ext = os.path.splitext(file)
    im = Image.open(file)
    width = im.size[0]
    height = im.size[1]
    print height

    crop_list = []
    while im.size[1] > 4000:
        box1 = (0, 0, im.size[0], 4000)
        crop_list.append(im.crop(box1))

        box2 = (0, 4000, im.size[0], im.size[1])
        im = im.crop(box2)
        print im.size[1]
    crop_list.append(im)

    count = 0
    for crop_image in crop_list:
        count += 1
        #print crop_image
        #crop_image.show()
        crop_image.save("images/" + filename + "_" + str(count) + ext)


def abcd(file):
    print file

# 전달인자
input_name = sys.argv[1]

# 파일 나누기 함수
split(input_name)
# thumbnail(input_name)

