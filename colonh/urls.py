# -*- encoding:utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from colonh.settings import STATIC_ROOT, MEDIA_ROOT

from django.conf import settings
from django.conf.urls.static import static



# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

basepatterns = patterns('',
    # Examples:
    # url(r'^$', 'colonh.views.home', name='home'),
    # url(r'^colonh/', include('colonh.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    # editor
    url(r'^ckeditor/', include('ckeditor.urls')),

    
    # registration
    url(r'^accounts/', include('registration.backends.default.urls', namespace='accounts')),

    # apps url    
    # $하면 뒤에 다른거 추가한거랑 다르게 취급됨
    url(r'^about/$', 'Information.views.about'),
    url(r'^editors/$', 'Information.views.editors'),
    url(r'^recruit/$', 'Information.views.recruit'),
    url(r'^contact/$', 'Information.views.contact'),
    
    url(r'^$', 'Volume.views.issue', name='main'),
    url(r'^issue/(\d+)/$', 'Volume.views.issue', name='issue'),
    url(r'^issue/(\d+)/(\w+)', 'Volume.views.issue_item', name='issue_item'),
    url(r'^issues/monthly/$', 'Volume.views.issues_monthly', name='issues_monthly'),
    url(r'^issues/parts/$', 'Volume.views.issues_parts', name='issues_parts'),
    url(r'^issues/parts/(\w+)$', 'Volume.views.issues_parts', name='issues_parts'),

    # news
    url(r'^news/$', 'news.views.news', name='news_all'),
    url(r'^news/(\w+)$', 'news.views.news', name='news'),
    url(r'^news/(\d+)$', 'news.views.news_item', name='news_item'),

    # board
    url(r'^board/qa/$', 'board.views.board_qa', name='board_qa'),
    url(r'^board/qa/(\d+)/$', 'board.views.board_qa_item', name='board_qa_item'),
    url(r'^board/qa/add/$', 'board.views.board_qa_add', name='board_qa_add'),
)
# 로컬이며 DEBUG모드일 경우
if settings.LOCAL and settings.DEBUG:
    print 'Local and DEBUG'
    urlpatterns = patterns('',
        url(r'^/', include(basepatterns)),
    )
    urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))
    
# 배포중이며 DEBUG모드일 경우
elif settings.DEBUG:
    print 'Deploy and Debug'
    urlpatterns = patterns('',
        url(r'^colonh/', include(basepatterns)),
    )
    urlpatterns += patterns('',
    (r'^colonh/media/(?P<path>.*)$', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))

# 배포중이며 DEBUG가 아닐 경우
else:
    print 'Deploy and NotDebug'
    urlpatterns = patterns('',
        url(r'^colonh/', include(basepatterns)),
    )