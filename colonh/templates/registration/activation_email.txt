{% load humanize %}
Colon_H 계정 활성화 안내 메일입니다.

안녕하세요, ColonH입니다.
계정을 활성화 하시려면 아래 링크를 클릭해주세요.
http://www.colonh.co.kr/accounts/activate/{{ activation_key }}/

{{ expiration_days|apnumber }}일 후에는 기간이 만료되어 링크가 비활성화됩니다.