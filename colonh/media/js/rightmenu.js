$(document).ready(function(){
    var isMenuOpen = false;
    var isDropdownMenuOpen = false;
    var pos = $("#right-menu-toggle").offset();
    var screen_width = $('body').width();
    var menu_width = $(".right-issue-menu").width() + 30;
    $("#right-issue-dropdown-box").hide();

    $("#right-menu-toggle").click(function(){
        if(isMenuOpen){
            $("#right-menu").animate({'width': 270}, 500);
            isMenuOpen = false;
        } else{
            $("#right-menu").animate({'width': 540}, 300);
            isMenuOpen = true;
        }
    });
    $(".right-issue-dropdown").click(function(){
        // $("#right-issue-dropdown-box").fadeIn(500);
        // $("#right-issue-dropdown-box").slideDown();
        if(isDropdownMenuOpen){
            $("#right-issue-dropdown-box").fadeOut(500);
            // $("#right-issue-dropdown-box").slideDown(500);
            isDropdownMenuOpen = false;
        } else{
            $("#right-issue-dropdown-box").fadeIn(500);   
            // $("#right-issue-dropdown-box").slideUp(500); 
            isDropdownMenuOpen = true;
        }
    });
});