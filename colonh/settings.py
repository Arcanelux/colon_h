# Django settings for colonh project.

import os
DOMAIN = 'http://iiii.so/colonh/'

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
print 'ROOT_PATH:' + ROOT_PATH
print 'PROJECT_PATH:' + PROJECT_PATH

STATIC_PATH = os.path.join(ROOT_PATH, 'static')
MEDIA_PATH = os.path.join(ROOT_PATH, 'media')
print 'STATIC_PATH:' + STATIC_PATH
print 'MEDIA_PATH:' + MEDIA_PATH

MEDIA_ROOT = MEDIA_PATH
MEDIA_URL = DOMAIN + 'media/'
STATIC_ROOT = ROOT_PATH + '/static_root'
STATIC_URL = '/static/'
print 'STATIC_ROOT:' + STATIC_ROOT

STATICFILES_DIRS = (
    STATIC_PATH,
)

LOCAL = True
if os.uname()[1] == 'arcanelux.vps.phps.kr':
    LOCAL = False
    STATIC_URL = DOMAIN + 'static/'
    MEDIA_URL = DOMAIN + 'media/'

# ckeditor
CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_PATH, 'ckeditor')
CKEDITOR_UPLOAD_PREFIX = DOMAIN + 'media/ckeditor/'
CKEDITOR_CONFIGS = {
    'awesome_ckeditor': {
        'toolbar': 'Basic',
    },
    'default': {
        'toolbar': 'Full',
        'height': 550,
        'width': '100%',
    },
}



DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
     ('lhy', 'test@test.com'),
)

# custom_user
AUTH_USER_MODEL = 'colon_member.ColonUser'
# AUTH_PROFILE_MODULE = 'custom_user.Profile'
# AUTH_USER_MODEL = 'custom_user.CustomUser'

# registration
ACCOUNT_ACTIVATION_DAYS=7
EMAIL_HOST='smtp.gmail.com'
EMAIL_PORT=587
EMAIL_HOST_USER='Webmaster.ColonH@gmail.com'
EMAIL_HOST_PASSWORD='colonh123'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = "Webmaster.ColonH@gmail.com"
DEFAULT_CHARSET = "utf-8"

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '/var/www/colonh/db.sqlite3',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Custom Apps




# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Seoul'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ko-kr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=#8ptwd0c*#xim^w9+dv!qz5v(p97+f+qhgv=2!(3+m#nrc^-d'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'colonh.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'colonh.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_PATH + '/templates',
)
# print "TEMPLATE_DIRS : " + PROJECT_PATH + '/templates'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'django.contrib.humanize',
    # 'cked',
    'ckeditor',
    'registration',
    # 'custom_user',
    'flynsarmy_paginator',
    'south',

    # apps
    'Information',
    'Volume',
    'colon_member',
    'board',
    'news',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
