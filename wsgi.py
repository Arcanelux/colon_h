import os
import site
import sys

site.addsitedir('/usr/local/lib/python2.7/site-packages')
os.environ['DJANGO_SETTINGS_MODULE'] = 'colonh.settings'

sys.path.insert(0, '/var/www/colonh')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()