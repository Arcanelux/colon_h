import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

path = '/srv/www'
if path not in sys.path:
    sys.path.insert(0, '/srv/www/')
    sys.path.append('/srv/www/colonh')

os.environ['DJANGO_SETTINGS_MODULE'] = 'colonh.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
