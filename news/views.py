from django.shortcuts import render_to_response

from news.models import *
from colonh.settings import MEDIA_URL

def news(request, category=''):
    # d = get_menu_info()
    d = {}

    if category != '':
        try:
            news_list = News.objects.filter(category__title=category.upper())
            d['active'] = category
        except:
            news_list = News.objects.all()
            d['active'] = 'all'
    else:
        news_list = News.objects.all()
        d['active'] = 'all'
        d['media_url'] = MEDIA_URL

    d['news_list'] = news_list

    return render_to_response('v2/news.html', d)

def news_item(request, pk):
    # d = get_menu_info()
    d = {}
    item = News.objects.get(pk=pk)

    d['item'] = item

    return render_to_response('v2/news_item.html', d)