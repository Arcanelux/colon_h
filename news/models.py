from django.db import models
from Volume.models import Part
from ckeditor.fields import RichTextField

# Category
# Title
# Content
# Part
# Created
class Category(models.Model):
    title = models.CharField(max_length=100)
    description = RichTextField()

    def __unicode__(self):
        return self.title


class News(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateField()
    content = RichTextField()
    part = models.ForeignKey(Part)
    category = models.ForeignKey(Category)
    attachment = models.FileField(upload_to='attachment/', blank=True, null=True)

    def __unicode__(self):
        return u"%s, %s" % (self.title, self.created)

